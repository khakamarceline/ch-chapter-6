const express = require("express");
const router = express.Router();
const users = require("../data/users.json");

router.get("/login", (req, res) => {
  res.render("login", { error: null });
});

router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const findUser = users.find(
    (user) => user.username === username && user.password === password
  );

  if (!findUser) {
    res.render("login", { error: "Invalid login" });
  }

  res.redirect("/transactions");
});

module.exports = router;
