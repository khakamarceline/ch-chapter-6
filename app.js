const express = require("express");
const app = express();
const port = 3000;
const { User, Transaction, Product } = require("./models");

const authenticate = require("./modules/authenticate");

app.use(express.urlencoded());
app.set("view engine", "ejs");

app.get("/", async (req, res) => {
  res.redirect("/login");
});

app.use(authenticate);

app.get("/transactions", async (req, res) => {
  const transactions = await Transaction.findAll({
    include: [User, Product],
  });
  res.render("transactions", { transactions: transactions });
});

app.get("/add-transactions", async (req, res) => {
  const users = await User.findAll({});
  res.render("add", { users: users });
});

app.post("/add-transactions", async (req, res) => {
  await Transaction.create(req.body);
  console.log("kesalahan", req.body);
  res.redirect("/transactions");
});

app.get("/edit-transactions/:id", async (req, res) => {
  const transaction = await Transaction.findOne({
    where: {
      id: req.params.id,
    },
  });
  res.render("edit", { transaction: transaction });
});

//update
app.post("/transactions", async (req, res) => {
  if (req.body._method) {
    await Transaction.update(req.body, {
      where: {
        id: req.body.id,
      },
    });
  } else {
    await Transaction.create(req.body);
  }
  res.redirect("/transactions");
});

//delete
app.post("/delete-transactions/:id", async (req, res) => {
  const id = req.params.id;
  await Transaction.destroy({
    where: {
      id,
    },
  });
  res.redirect("/transactions");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
